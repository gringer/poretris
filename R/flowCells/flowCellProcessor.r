library(png)
library(jpeg)
library(gifski)

boardImage <- readPNG("/bioinf/voluntary/twitter/poretris/poretris/flowCells/Flongle_cellphone.png");
boardImage <- readJPEG("/bioinf/voluntary/twitter/poretris/poretris/flowCells/Flongle_screenshot.jpg");
boardImage <- readJPEG("/bioinf/voluntary/twitter/poretris/poretris/flowCells/Flongle_screenshot_noDark.jpg");

makePlot <- function(){
  xAdj <- (boardImage[,2,2] + boardImage[,ncol(boardImage)-1,2])/2;
  yAdj <- (boardImage[2,,2] + boardImage[nrow(boardImage)-1,,2])/2;
  boardGreen <- boardImage;
  boardGreen[,,2] <- boardGreen[,,2] - xAdj;
  boardGreen[,,2] <- boardGreen[,,2] - rep(yAdj, each=nrow(boardGreen));
  boardGreen[,,2] <- boardGreen[,,2] - min(boardGreen[,,2]);
  boardGreen[,,2] <- boardGreen[,,2] / max(boardGreen[,,2]);
  par(mar=c(2.5,2.5,1,1), mgp=c(1.5,0.5,0));
  layout(matrix(1:6,nrow=2));
  dens.y.last <- density(boardGreen[2,,2], from=0, to=1);
  dens.x.last <- density(boardGreen[,2,2], from=0, to=1);
  ddiff.x <- c(100,sapply(2:ncol(boardImage),function(xpos){
    dens.x <- density(boardGreen[,xpos,2], from=0, to=1);
    dens.xm1 <- density(boardGreen[,xpos-1,2], from=0, to=1);
    sum(abs(dens.x$y - dens.xm1$y));
  }));
  ddiff.y <- c(100,sapply(2:nrow(boardImage),function(ypos){
    dens.y <- density(boardGreen[ypos,,2], from=0, to=1);
    dens.ym1 <- density(boardGreen[ypos-1,,2], from=0, to=1);
    sum(abs(dens.y$y - dens.ym1$y));
  }));
  adiff.x <- sapply(1:ncol(boardImage), function(xpos){
    max(apply(boardImage[,xpos,],1,max) - apply(boardImage[,xpos,],1,min));
  });
  adiff.y <- sapply(1:nrow(boardImage), function(ypos){
    max(apply(boardImage[ypos,,],1,max) - apply(boardImage[ypos,,],1,min));
  });
  diffT <- ifelse(min(boardGreen[,1:5,2] > 0.95), 200, 100);
  ## RLE simplification (X)
  blank.x.rle <- rle((ddiff.x >= diffT) | (adiff.x < 0.1));
  blank.x.rle$values[blank.x.rle$lengths <=4] <- TRUE;
  blank.x <- inverse.rle(blank.x.rle);
  blank.x.rle <- rle(blank.x);
  blank.x.rle$values[blank.x.rle$lengths < mean(blank.x.rle$lengths)] <- TRUE;
  blank.x <- inverse.rle(blank.x.rle);
  blank.x.rle <- rle(blank.x);
  bxmn <- min(blank.x.rle$lengths[!blank.x.rle$values] - 2)/2;
  bxmx <- max(blank.x.rle$lengths[!blank.x.rle$values] - 2)/2;
  blank.x.midPoints <- 
    (cumsum(blank.x.rle$lengths) - blank.x.rle$lengths/2)[!blank.x.rle$values];
  ## RLE simplification (Y)
  blank.y.rle <- rle((ddiff.y >= diffT) | (adiff.y < 0.1));
  blank.y.rle$values[blank.y.rle$lengths <=4] <- TRUE;
  blank.y <- inverse.rle(blank.y.rle);
  blank.y.rle <- rle(blank.y);
  blank.y.rle$values[blank.y.rle$lengths < mean(blank.y.rle$lengths)] <- TRUE;
  blank.y <- inverse.rle(blank.y.rle);
  blank.y.rle <- rle(blank.y);
  bymn <- min(blank.y.rle$lengths[!blank.y.rle$values] - 2)/2;
  bymx <- max(blank.y.rle$lengths[!blank.y.rle$values] - 2)/2;
  blank.y.midPoints <- 
    (cumsum(blank.y.rle$lengths) - blank.y.rle$lengths/2)[!blank.y.rle$values];
  ## Fill in any gaps
  if(max(diff(blank.x.midPoints)) > (1.2 * min(diff(blank.x.midPoints)))){
    ideal.gapWidth <- median(diff(blank.x.midPoints));
    numPoints <- round(diff(range(blank.x.midPoints)) / ideal.gapWidth + 1);
    ideal.points <- seq(min(blank.x.midPoints),max(blank.x.midPoints),
                        length.out = numPoints);
    for(xi in seq_along(ideal.points)){
      x <- ideal.points[xi];
      if(any(abs(blank.x.midPoints - x) < ideal.gapWidth/4)){
        ideal.points[xi] <- 
          blank.x.midPoints[head(which(abs(blank.x.midPoints - x) < ideal.gapWidth/4),1)];
      }
    }
    blank.x.midPoints <- ideal.points;
  }
  if(max(diff(blank.y.midPoints)) > (1.2 * min(diff(blank.y.midPoints)))){
    ideal.gapWidth <- median(diff(blank.y.midPoints));
    numPoints <- round(diff(range(blank.y.midPoints)) / ideal.gapWidth + 1);
    ideal.points <- seq(min(blank.y.midPoints),max(blank.y.midPoints),
                        length.out = numPoints);
    for(xi in seq_along(ideal.points)){
      x <- ideal.points[xi];
      if(any(abs(blank.y.midPoints - x) < ideal.gapWidth/4)){
        ideal.points[xi] <- 
          blank.y.midPoints[head(which(abs(blank.y.midPoints - x) < ideal.gapWidth/4),1)];
      }
    }
    blank.y.midPoints <- ideal.points;
  }
  for(yp in blank.y.midPoints){
    for(xp in blank.x.midPoints){
      boardGreen[(yp-bymx):(yp+bymx),(xp-bxmx):(xp+bxmx),1] <-
        mean(boardGreen[(yp-bymn):(yp+bymn),(xp-bxmn):(xp+bxmn),1]);
      boardGreen[(yp-bymx):(yp+bymx),(xp-bxmx):(xp+bxmx),2] <-
        mean(boardGreen[(yp-bymn):(yp+bymn),(xp-bxmn):(xp+bxmn),2]);
      boardGreen[(yp-bymx):(yp+bymx),(xp-bxmx):(xp+bxmx),3] <-
        mean(boardGreen[(yp-bymn):(yp+bymn),(xp-bxmn):(xp+bxmn),3]);
    }
  }
  wells.df <- data.frame(x=rep(blank.x.midPoints, length(blank.y.midPoints)),
                          y=rep(blank.y.midPoints, each=length(blank.x.midPoints)));
  wells.df$red <- boardGreen[cbind(wells.df$y, wells.df$x, 1)]; 
  wells.df$green <- boardGreen[cbind(wells.df$y, wells.df$x, 2)]; 
  wells.df$blue <- boardGreen[cbind(wells.df$y, wells.df$x, 3)];
  ## Further gradient adjustment for green
  greenModel <- glm(green ~ y + red + blue + x, data=wells.df);
  yGradient <- greenModel$coefficients["y"];
  xGradient <- greenModel$coefficients["x"];
  wells.df$green <- wells.df$green - wells.df$y * yGradient;
  wells.df$green <- wells.df$green - wells.df$x * xGradient;
  ## Classify pores
  wells.df$minrgb <- apply(wells.df[,c("red","green","blue")],1,min);
  wells.df$maxrgb <- apply(wells.df[,c("red","green","blue")],1,max);
  wells.df$pore <- ifelse((wells.df$green == wells.df$maxrgb),TRUE,FALSE);
  wells.df$empty <- ifelse((wells.df$blue == wells.df$maxrgb),TRUE,FALSE);
  wells.df$classification <- as.character("E");
  wells.df$classification[wells.df$pore] <- "S";
  ## distinguish between light (single) and dark green (unstranded)
  wdp <- wells.df[wells.df$pore,"green"];
  med.s <- median(wdp);
  mad.s <- mad(wdp);
  if(any(wdp < (med.s - 3*mad.s))){
    wells.df$classification[which(wells.df$pore)[
      wdp < (med.s - 3*mad.s)]] <- "U";
  }
  if(any(wdp > (med.s - 3*mad.s))){
    wells.df$classification[which(wells.df$pore)] <- "U";
    wells.df$classification[which(wells.df$pore)[
      wdp > (med.s - 3*mad.s)]] <- "S";
  }
  ## distinguish between light (empty) and dark blue (recovering)
  wde <- wells.df[wells.df$empty,"green"];
  med.e <- median(wde);
  mad.e <- mad(wde);
  if(any(wde < (med.e - 3*mad.e))){
    wells.df$classification[which(wells.df$empty)[
      wde < (med.e - 3*mad.e)]] <- "R";
  }
  if(any(wde > (med.e - 3*mad.e))){
    wells.df$classification[which(wells.df$empty)] <- "R";
    wells.df$classification[which(wells.df$empty)[
      wde < (med.e - 3*mad.e)]] <- "E";
  }
  ## Grey spaces are considered empty
  wells.df$classification[wells.df$maxrgb - wells.df$minrgb < 0.05] <- "E";
  for(frac in seq(0, 1, length.out=120)){
    print(frac);
    xpos <- frac * (ncol(boardImage)-2) + 2;
    ypos <- frac * (nrow(boardImage)-2) + 2;
    plot(NA,xlim=c(1,ncol(boardImage)), ylim=c(nrow(boardImage),1),
         xlab="X Pos", ylab="Y Pos");
    rasterImage(boardImage,1, nrow(boardImage), ncol(boardImage), 1);
    abline(h=ypos, col="#FF000080", lwd=3);
    abline(v=xpos, col="#0040FF80", lwd=3);
    plot(x=runmed(boardGreen[,xpos,2],5), 
         y=nrow(boardGreen):1, xlab="Green Value",
         ylab="Y Pos", xlim=c(0,1), type="l", lwd=3, col="#0040FF80");
    dens.y <- density(boardGreen[ypos,,2], from=0, to=1);
    dens.x <- density(boardGreen[,xpos,2], from=0, to=1);
    plot(dens.y);
    plot(dens.x);
    plot(x=1:ncol(boardGreen), y=runmed(boardGreen[ypos,,2],5), xlab="X Pos",
         ylab="Green Value", ylim=c(0,1), type="l", lwd=3, col="#FF000080");
    plot(NA,xlim=c(1,ncol(boardGreen)), ylim=c(nrow(boardGreen),1),
         xlab="X Pos", ylab="Y Pos");
    rasterImage(boardGreen,1, nrow(boardGreen), ncol(boardGreen), 1);
    text(x=wells.df$x, 
         y=wells.df$y, labels=wells.df$classification, col="yellow",
         cex = 0.75);
    abline(h=ypos, col="#FF000080", lwd=3);
    abline(v=xpos, col="#0040FF80", lwd=3);
  }
}

save_gif(makePlot(), width = 900, height = 600, res = 96, delay=0.15);

boardRed <- boardGreen;
boardRed[,,2] <- boardRed[,,1];
boardRed[,,3] <- boardRed[,,1];

boardBlue <- boardGreen;
boardBlue[,,1] <- boardBlue[,,3];
boardBlue[,,2] <- boardBlue[,,3];

png("classified_cellphone.png", width=1000, height=500);
par(mfrow=c(1,2), mar=c(2.5,2.5,1,1), mgp=c(1.5,0.5,0));
plot(NA,xlim=c(1,ncol(boardImage)), ylim=c(nrow(boardImage),1),
     xlab="X Pos", ylab="Y Pos");
rasterImage(boardImage,1, nrow(boardImage), ncol(boardImage), 1);
plot(NA,xlim=c(1,ncol(boardGreen)), ylim=c(nrow(boardGreen),1),
     xlab="X Pos", ylab="Y Pos");
rasterImage(boardGreen,1, nrow(boardGreen), ncol(boardGreen), 1);
text(x=wells.df$x, 
     y=wells.df$y, labels=wells.df$classification, col="yellow",
     cex = 1.5);
invisible(dev.off());