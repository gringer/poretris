extends TileMap

const BASES_PER_SECOND = 450.0
const ADAPTER_LENGTH = 50.0
const STAT_UPDATE_PERIOD = 1.0
const STARTING_OCCUPANCY = 0.5
const MEAN_SEQ_LENGTH = 13500 # 30s at 450 bases / second
const MEAN_TRANSITION_TIME = MEAN_SEQ_LENGTH / BASES_PER_SECOND
const TRANSITION_SD = MEAN_TRANSITION_TIME * 0.1 # 10% CV
const START_HEIGHT = 13
const CELL_BLANK = Vector2i(-1, -1)
const CELL_EMPTY_BASIC = Vector2i(4, 1)
const CELL_EMPTY_EXPANDED = Vector2i(7, 0)
const CELL_WALL = Vector2i(0, 2)
const CELL_SEQUENCING_BASIC = Vector2i(1,1)
const CELL_SINGLE_BASIC = Vector2i(2,1)
const CELL_UNAVAILABLE_BASIC = Vector2i(3,1)
const PORE_PROGRESSION = [CELL_EMPTY_BASIC, CELL_UNAVAILABLE_BASIC, CELL_SINGLE_BASIC, CELL_SEQUENCING_BASIC]
const PORE_PROG_LOOKUP = {"(4, 1)": 0, "(3, 1)": 1, "(2, 1)": 2, "(1, 1)": 3}
const ShapeNames = ["L","J","O","S0","Z0","T","I0"]
const PieceRotationName = {"S0": "S1", "S1" : "S0", "Z0": "Z1", "Z1": "Z0",
						   "I0": "I1", "I1" : "I0",
						   "L": "L", "J": "J", "O": "O", "T": "T"}
const TetrisShapes = {
	"L" : [[0,0,0,0,0],
		   [0,0,1,0,0],
		   [0,0,1,0,0],
		   [0,0,1,1,0],
		   [0,0,0,0,0]],
	"J" : [[0,0,0,0,0],
		   [0,0,1,0,0],
		   [0,0,1,0,0],
		   [0,1,1,0,0],
		   [0,0,0,0,0]],
	"O" : [[0,0,0,0,0],
		   [0,0,0,0,0],
		   [0,0,1,1,0],
		   [0,0,1,1,0],
		   [0,0,0,0,0]],
	"S0" : [[0,0,0,0,0],
		   [0,0,0,0,0],
		   [0,0,1,1,0],
		   [0,1,1,0,0],
		   [0,0,0,0,0]],
	"S1" : [[0,0,0,0,0],
		   [0,1,0,0,0],
		   [0,1,1,0,0],
		   [0,0,1,0,0],
		   [0,0,0,0,0]],
	"Z0" : [[0,0,0,0,0],
		   [0,0,0,0,0],
		   [0,1,1,0,0],
		   [0,0,1,1,0],
		   [0,0,0,0,0]],
	"Z1" : [[0,0,0,0,0],
		   [0,0,0,1,0],
		   [0,0,1,1,0],
		   [0,0,1,0,0],
		   [0,0,0,0,0]],
	"T" : [[0,0,0,0,0],
		   [0,0,0,0,0],
		   [0,1,1,1,0],
		   [0,0,1,0,0],
		   [0,0,0,0,0]],
	"I0" : [[0,0,0,0,0],
		   [0,0,0,0,0],
		   [0,1,1,1,1],
		   [0,0,0,0,0],
		   [0,0,0,0,0]],
	"I1" : [[0,0,0,0,0],
		   [0,0,1,0,0],
		   [0,0,1,0,0],
		   [0,0,1,0,0],
		   [0,0,1,0,0]]
		}

var array_minX = 10
var array_minY = 1
var array_maxX = 19
var array_maxY = 26
var array_midX = int((array_minX + array_maxX) >> 1)
var array_nextX = 22
var array_nextY = 5
var throttle_x: float = 0
var throttle_y: float = 0
var dropTime: float = 2
var timeToNextDrop: float = 0
var fastDrop = true
var shouldClearLines = false
var gamePaused = false

var countSequencing = 0
var countSingle = 0
var countUnavailable = 0
var scoreTotal = 0
var poreSeconds: float = 0.0
var statUpdateRemaining = STAT_UPDATE_PERIOD
var gameFieldDelta = 0
var gameField = Array()

@onready var gameMap: TileMap = self
@onready var root: Node2D = self.get_parent()
@onready var nextPieceBG: ColorRect = root.get_node("NextBG")
@onready var nextPieceMap: TileMap = root.get_node("NextBG/NextPieceField")
@onready var nextPieceName = ShapeNames[randi() % ShapeNames.size()]
@onready var nextPiece = TetrisShapes[nextPieceName]
@onready var nextPieceMapOffset = nextPieceMap.global_position
@onready var labelCountSequencing: Label = $CountSequencing
@onready var labelCountSingle: Label = $CountSingle
@onready var labelCountUnavailable: Label = $CountUnavailable
@onready var labelScoreTotal: Label = $ScoreTotal
@onready var labelScoreBases: Label = $ScoreBaseEstimate

var dropPieceName
var dropPiece
var pieceOffsetX: int
var pieceOffsetY: int

func valToSci(val, unit = ""):
	var sv = sign(val)
	val = abs(val)
	var sciPrefixes = ["", "k", "M", "G", "T", "P", "E", "Z", "Y"]
	var units = Array()
	var decades = Array()
	for sciPortion in sciPrefixes.size():
		var prefix = sciPrefixes[sciPortion]
		for i in range(3):
			units.append(prefix + " " + unit)
			decades.append(10.0 ** (sciPortion * 3.0))
	var logRegion = floor(log(val)/log(10))+1
	var convUnits = units[logRegion]
	var convVal = round(val * 1000.0 / decades[logRegion]) / 1000.0
	return(str(sv * convVal) + convUnits)

func updateGameField(delta: float):
	#var poreOccupancy = float(countSequencing) / (countSequencing + countSingle)
	var poreOccupancy = STARTING_OCCUPANCY
	var meanReloadTime = (MEAN_TRANSITION_TIME / poreOccupancy) - MEAN_TRANSITION_TIME
	var reloadSD = meanReloadTime * 0.1 # 10% coefficient of variation
	for fieldY in range(array_minY, array_maxY +1):
		for fieldX in range(array_minX, array_maxX + 1):
			var fieldPos = Vector2i(fieldX, fieldY)
			var atlasCoords = gameMap.get_cell_atlas_coords(0, fieldPos)
			if(atlasCoords == CELL_WALL):
				continue
			atlasCoords = gameMap.get_cell_atlas_coords(1, fieldPos)
			if((atlasCoords == CELL_BLANK) || (atlasCoords == CELL_UNAVAILABLE_BASIC)):
				# don't update timings for blank / unavailable cells
				continue
			var remainingTime = gameField[fieldY][fieldX]
			remainingTime -= delta
			if(remainingTime <= 0): # pore changed state in the time since last check
				remainingTime -= delta
				if(atlasCoords == CELL_SEQUENCING_BASIC): # transition from seq to not seq
					remainingTime += randfn(meanReloadTime, reloadSD)
					gameMap.set_cell(1, fieldPos, 0, CELL_SINGLE_BASIC)
				elif(atlasCoords == CELL_SINGLE_BASIC): # transition from not seq to seq
					remainingTime += randfn(MEAN_TRANSITION_TIME, TRANSITION_SD)
					gameMap.set_cell(1, fieldPos, 0, CELL_SEQUENCING_BASIC)
				if(remainingTime <= 0): # don't allow more than one transition
					remainingTime = 0
				gameField[fieldY][fieldX] = remainingTime
			else:
				gameField[fieldY][fieldX] -= delta

func checkAndClearLines():
	shouldClearLines = false
	var clearedLines = false
	var lineClearCount = 0
	var scoreSubTotal = 0
	for fieldY in range(array_maxY, array_minY - 1, -1):
		var shiftPositions: Array = Array()
		# Count available cells for removal
		for fieldX in range(array_minX, array_maxX + 1):
			var atlasCoords = gameMap.get_cell_atlas_coords(0, Vector2i(fieldX, fieldY))
			if(atlasCoords != CELL_WALL):
				shiftPositions.append(fieldX)
		var filledCount = 0
		# Check emptiness of available cells
		for fieldX in shiftPositions:
			var atlasCoords = gameMap.get_cell_atlas_coords(1, Vector2i(fieldX, fieldY))
			if((atlasCoords != CELL_EMPTY_BASIC)
					&& (atlasCoords != CELL_EMPTY_EXPANDED)
					&& (atlasCoords != CELL_BLANK)):
				filledCount += 1
		# Shift above cells down 1
		if(filledCount == shiftPositions.size()):
			lineClearCount += 1
			clearedLines = true
			var foundPore = false
			for shiftX in shiftPositions:
				var atlasCoords = gameMap.get_cell_atlas_coords(1, Vector2i(shiftX, fieldY))
				if(PORE_PROG_LOOKUP[str(atlasCoords)] > 1):
					foundPore = true
					scoreSubTotal += (2 << PORE_PROG_LOOKUP[str(atlasCoords)])
				gameMap.set_cell(1, Vector2i(shiftX, fieldY), 0,
					PORE_PROGRESSION[PORE_PROG_LOOKUP[str(atlasCoords)]-1])
			if(!foundPore):
				for shiftX in shiftPositions:
					for shiftY in range(fieldY, array_minY, -1):
						var atlasCoords = gameMap.get_cell_atlas_coords(1, Vector2i(shiftX, shiftY-1))
						gameMap.set_cell(1, Vector2i(shiftX, shiftY), 0, atlasCoords)
	scoreTotal += scoreSubTotal * lineClearCount
	if(clearedLines):
		checkAndClearLines()
	else:
		countSequencing = 0
		countSingle = 0
		countUnavailable = 0
		for fieldY in range(array_maxY, array_minY - 1, -1):
			for fieldX in range(array_minX, array_maxX + 1):
				var atlasCoords = gameMap.get_cell_atlas_coords(1, Vector2i(fieldX, fieldY))
				if(atlasCoords == CELL_SEQUENCING_BASIC):
					countSequencing += 1
				if(atlasCoords == CELL_SINGLE_BASIC):
					countSingle += 1
				if(atlasCoords == CELL_UNAVAILABLE_BASIC):
					countUnavailable += 1
		labelCountSequencing.text = str(countSequencing)
		labelCountSingle.text = str(countSingle)
		labelCountUnavailable.text = str(countUnavailable)
		labelScoreTotal.text = str(scoreTotal)
	if((countSequencing == 0) && (countSingle == 0)):
		winGame()

func drawNextPiece():
	nextPieceMap.clear_layer(0)
	var nextMinX = 6
	var nextMaxX = -1
	var nextMinY = 6
	var nextMaxY = -1
	for dropX in range(5):
		for dropY in range(5):
			if(nextPiece[dropY][dropX] == 1):
				nextMinX = dropX if (dropX < nextMinX) else nextMinX
				nextMinY = dropY if (dropY < nextMinY) else nextMinY
				nextMaxX = dropX if (dropX > nextMaxX) else nextMaxX
				nextMaxY = dropY if (dropY > nextMaxY) else nextMaxY
	var offsetX = -(nextMinX + nextMaxX + 1) / 2.0 * nextPieceMap.tile_set.tile_size.x * nextPieceMap.scale.x
	var offsetY = -(nextMinY + nextMaxY + 1) / 2.0 * nextPieceMap.tile_set.tile_size.y * nextPieceMap.scale.y
	for dropX in range(5):
		for dropY in range(5):
			if(nextPiece[dropY][dropX] == 1):
				nextPieceMap.set_cell(0, Vector2i(dropX, dropY), 1, Vector2i(3, 1))
	nextPieceMap.global_position = Vector2(offsetX, offsetY) + nextPieceMapOffset

func drawPiece():
	gameMap.clear_layer(2)
	for dropX in range(5):
		for dropY in range(5):
			if(dropPiece[dropY][dropX] == 1):
				gameMap.set_cell(2, Vector2i(array_minX + pieceOffsetX + dropX,
					array_minY + pieceOffsetY + dropY), 0, Vector2i(3, 1))

# returns true if shape movement is possible in the requested direction
func checkShapeMove(xDelta: int, yDelta: int, ignorePlaced: bool = false):
	for dropX in range(5):
		for dropY in range(5):
			if(dropPiece[dropY][dropX] == 1):
				# check walls
				var atlasCoords = gameMap.get_cell_atlas_coords(0,
					Vector2i(array_minX + pieceOffsetX + xDelta + dropX,
					array_minY + pieceOffsetY + yDelta + dropY))
				if((atlasCoords != CELL_EMPTY_BASIC) && (atlasCoords != CELL_EMPTY_EXPANDED)):
					return false
				# check placed pieces
				if(!ignorePlaced):
					atlasCoords = gameMap.get_cell_atlas_coords(1,
						Vector2i(array_minX + pieceOffsetX + xDelta + dropX,
						array_minY + pieceOffsetY + yDelta + dropY))
					if((atlasCoords != CELL_EMPTY_BASIC)
					&& (atlasCoords != CELL_EMPTY_EXPANDED)
					&& (atlasCoords != CELL_BLANK)):
						return false
	return true

func loseGame():
	var endDialog : AcceptDialog = $GameEndDialog
	endDialog.get_window().title = "Lost PoreTris"
	endDialog.dialog_text = "Unfortunately, it looks like you have lost PoreTris.\n" +\
		"Hopefully you can find it and play again sometime.\n" +\
		"Happy sequencing!"
	endDialog.popup_centered()
	gamePaused = true

func winGame():
	var endDialog : AcceptDialog = $GameEndDialog
	endDialog.get_window().title = "Won PoreTris"
	endDialog.dialog_text = "You have won PoreTris!\n" +\
		"Happy sequencing!"
	endDialog.popup_centered()
	gamePaused = true

func pauseGame():
	var pauseDialog : AcceptDialog = $PauseDialog
	pauseDialog.get_window().title = "Paused Game"
	pauseDialog.dialog_text = "Game Paused"
	pauseDialog.popup_centered()
	gamePaused = true

# Prepares a new piece to drop
func setupNextPiece():
	dropPiece = nextPiece
	dropPieceName = nextPieceName
	nextPieceName = ShapeNames[randi() % ShapeNames.size()]
	nextPiece = TetrisShapes[nextPieceName]
	var rotateTimes = randi() % 4
	for i in range(rotateTimes):
		nextPiece = rotate_cw(nextPieceName, nextPiece)
		nextPieceName = PieceRotationName[nextPieceName]
	pieceOffsetX = 4
	pieceOffsetY = 0
	timeToNextDrop = dropTime
	while(checkShapeMove(0, -1, true)):
		pieceOffsetY -= 1
	if(!checkShapeMove(0,0)):
		loseGame()
	if(checkShapeMove(0,1)):
		pieceOffsetY += 1
	drawPiece()
	drawNextPiece()

# Adds a starting field to make the game more interesting
func makeRandomField():
	gameField = Array()
	for fieldY in range(0, array_maxY + 1):
		var fieldLine = Array()
		for fieldX in range(0, array_maxX + 1):
			fieldLine.append(0)
		gameField.append(fieldLine)
	for fieldY in range(array_maxY, array_maxY - START_HEIGHT, -1):
		var fieldLineTimes = Array()
		var emptiesAdded = 0
		var slotsRemaining = 0
		for fieldX in range(array_minX, array_maxX + 1):
			var atlasCoords = gameMap.get_cell_atlas_coords(0, Vector2i(fieldX, fieldY))
			if(atlasCoords != CELL_WALL):
				fieldLineTimes.append(0)
				slotsRemaining += 1
			else:
				fieldLineTimes.append(-1)
		for fieldX in range(array_minX, array_maxX + 1):
			var atlasCoords = gameMap.get_cell_atlas_coords(0, Vector2i(fieldX, fieldY))
			if(atlasCoords != CELL_WALL):
				var transitionTime = 0
				var poreType = randi() % (PORE_PROGRESSION.size() - 1) + 1;
				# make sure at least one empty is added in each row, but about 1/4 of pores
				if((randi() % slotsRemaining) < ((slotsRemaining - emptiesAdded) * 0.25)):
					poreType = 0
				var chosenTile = PORE_PROGRESSION[poreType]
				if(chosenTile == CELL_SEQUENCING_BASIC): # start of a sequence
					transitionTime = randfn(randf() * MEAN_TRANSITION_TIME, TRANSITION_SD)
					if(transitionTime < 0): # no sequence, just adapter
						transitionTime = ADAPTER_LENGTH / BASES_PER_SECOND
				else:
					## TODO: more reasonable default for non-sequencing pores
					transitionTime = randfn(randf() * MEAN_TRANSITION_TIME, TRANSITION_SD)
					if(transitionTime < 0): # no sequence, just adapter
						transitionTime = ADAPTER_LENGTH / BASES_PER_SECOND
				gameField[fieldY][fieldX] = transitionTime
				gameMap.set_cell(1, Vector2i(fieldX, fieldY), 0, chosenTile);
	checkAndClearLines()

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	makeRandomField()
	setupNextPiece()

# Locks the current piece into position
func lockPiece():
	gameMap.clear_layer(2)
	for dropX in range(5):
		for dropY in range(5):
			if(dropPiece[dropY][dropX] == 1):
				gameMap.set_cell(1, Vector2i(array_minX + pieceOffsetX + dropX,
					array_minY + pieceOffsetY + dropY), 0, Vector2i(3, 1))
	shouldClearLines = true
	setupNextPiece()

# moves the shape in the requested direction (if possible)
func moveShape(xDelta: int, yDelta: int):
	if(checkShapeMove(xDelta, yDelta)):
		pieceOffsetX += xDelta
		pieceOffsetY += yDelta
		drawPiece()

func moveShapeDown():
	if(checkShapeMove(0, 1)):
		pieceOffsetY += 1
		drawPiece()
	else:
		lockPiece()

# moves the shape down until it can't be moved any more
func dropShape():
	while(checkShapeMove(0, 1)):
		pieceOffsetY += 1
	drawPiece()

# rotates the piece counter-clockwise
func rotate_ccw(pieceName: String, pieceToRotate: Array):
	if(pieceName == "O"):
		return(pieceToRotate)
	elif((pieceName.begins_with("I")) || (pieceName.begins_with("S")) || (pieceName.begins_with("Z"))):
		# tricky rotation due to symmetry
		return(TetrisShapes[PieceRotationName[pieceName]])
	else:
		var newPiece = pieceToRotate.duplicate(true)
		for yp in range(5):
			for xp in range(5):
				newPiece[4-yp][xp] = pieceToRotate[xp][yp]
		return(newPiece)

# rotates the piece clockwise
func rotate_cw(pieceName: String, pieceToRotate):
	if(pieceName == "O"): # no rotation
		return(pieceToRotate)
	elif((pieceName.begins_with("I")) || (pieceName.begins_with("S")) || (pieceName.begins_with("Z"))):
		# tricky rotation due to symmetry
		return(TetrisShapes[PieceRotationName[pieceName]])
	else:
		var newPiece = pieceToRotate.duplicate(true)
		for xp in range(5):
			for yp in range(5):
				newPiece[xp][yp] = pieceToRotate[4-yp][xp]
		return(newPiece)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(Input.is_action_pressed("ui_cancel")):
		get_tree().quit()
	if(Input.is_action_just_pressed("pause_game")):
		pauseGame()
	if(gamePaused):
		return
	if(shouldClearLines):
		checkAndClearLines()
	if(Input.is_action_pressed("move_left") && (throttle_x <= 0)):
		moveShape(-1, 0)
		throttle_x = 0.2
	if(Input.is_action_pressed("move_right") && (throttle_x <= 0)):
		moveShape(1, 0)
		throttle_x = 0.2
	if(Input.is_action_just_pressed("rotate_ccw")):
		dropPiece = rotate_ccw(dropPieceName, dropPiece)
		dropPieceName = PieceRotationName[dropPieceName]
		if(!checkShapeMove(0, 0)):
			dropPiece = rotate_cw(dropPieceName, dropPiece)
			dropPieceName = PieceRotationName[dropPieceName]
		else:
			drawPiece()
	if(Input.is_action_just_pressed("rotate_cw")):
		dropPiece = rotate_cw(dropPieceName, dropPiece)
		dropPieceName = PieceRotationName[dropPieceName]
		if(!checkShapeMove(0, 0)):
			dropPiece = rotate_ccw(dropPieceName, dropPiece)
			dropPieceName = PieceRotationName[dropPieceName]
		else:
			drawPiece()
	if(Input.is_action_just_pressed("move_down")):
		if(checkShapeMove(0, 1)):
			timeToNextDrop = dropTime
		else:
			timeToNextDrop = 0
		moveShapeDown()
	if(Input.is_action_just_pressed("drop_piece")):
		if(!fastDrop && checkShapeMove(0, 1)):
			timeToNextDrop = (dropTime / 2)
		else:
			timeToNextDrop = 0
		dropShape()
	timeToNextDrop -= delta
	if(timeToNextDrop <= 0):
		moveShapeDown()
		timeToNextDrop = dropTime + timeToNextDrop
	if(throttle_x > 0):
		throttle_x -= delta
	if(!Input.is_action_pressed("move_left") and !Input.is_action_pressed("move_right")):
		throttle_x = 0
	gameFieldDelta += delta
	poreSeconds += delta * countSequencing
	statUpdateRemaining -= delta
	if(statUpdateRemaining <= 0):
		var basesSequenced = poreSeconds * BASES_PER_SECOND
		updateGameField(gameFieldDelta)
		gameFieldDelta = 0
		labelScoreBases.text = valToSci(round(basesSequenced), "b")
		statUpdateRemaining = STAT_UPDATE_PERIOD - statUpdateRemaining



func _on_game_end_dialog_confirmed():
	get_tree().quit()
	pass # Replace with function body.

func _on_pause_dialog_confirmed():
	gamePaused = false
